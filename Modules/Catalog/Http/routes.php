<?php

Route::group(['middleware' => 'web', 'prefix' => 'catalogo', 'namespace' => 'Modules\Catalog\Http\Controllers'], function()
{
    Route::get('/', 'CatalogController@index');
    Route::get('/show/{id}','CatalogController@show')->where('id','[0-9]+');
    Route::get('/create','CatalogController@create');
    Route::get('/edit/{id}','CatalogController@edit')->where('id','[0-9]+');
    Route::put('edit/{id}','CatalogController@update')->where('id','[0-9]+');
    Route::post('catalog/create','CatalogController@store');
});
